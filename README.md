# winbut

It's a tiny minimalistic autohiding panel that shows active window's Icon, Title and Control Buttons.

It's especially useful for maximized windows if they don't show their own titlebars.

This program is written in Python + GTK3 for Linux Desktop Environments (X11). (I prefer XFCE Desktop).

You can choose to not show the active window's title (as well as choose to not display the icon or the buttons), then you could see the title in the tooltip. Screenshot (shows the active window's icon and control buttons; the title is hidden in the window but shown in the tooltip):

![screenshot](https://codeberg.org/chang-zhao/winbut/raw/branch/main/doc/winbut1.webp)

There is a [plugin for XFCE Panel](https://gitlab.xfce.org/panel-plugins/xfce4-windowck-plugin) with similar functionality, but it's buggy (often crashes or doesn't refresh timely), probably because of some ancient methods, and it lacks some functionality like buttons theming, and it might require to add a whole panel to the desktop. This program has all the functionality well working (AFAIK) and is standalone, not requiring a panel. It has extensive settings and is written in Python, the simplest and the most popular programming language; so you can easily read and modify it.

All the best to all sentient beings!

## Installation and setup

1. Download it and unpack into any folder. Mark `winbut` executable if necessary.
2. `gobject-introspection` (or at least `pygobject`) should be installed [to run GTK3 with Python](https://python-gtk-3-tutorial.readthedocs.io/en/latest/install.html).

To change settings, edit `./conf.py` (screen position, size, autohide, which info to show etc.). Default settings should work fine though.

To change styles, edit CSS in `./main.css`

To start it at login, set this command in autostart (Settings -> Session and Startup):

```
/bin/sh -c 'sleep 1; <path-to>/winbut &'
```

where `<path-to>/winbut` is a path to your `winbut` program. That 1 second delay will ensure that the Desktop is already completely initialized when winbut starts. In case something still goes wrong, you can run `winbut` yet another time, and it will kill any previously started instance.

## Usage

* Clicking Control Buttons (minimize, maximize, close) controls the currently active window.
* Clicking the Active Window Icon pins or unpins the program, i.e. switches autohiding on the fly.
* Clicking Title area switches showing the active program title (or only its 1st letter).
* To quit the program, `pkill winbut` from CLI or use Task Manager.

## Debugging

```
GTK_DEBUG=interactive /usr/bin/python ./winbut
```