####################################
# Position on screen, width & height

# Orientation should be
#   "v" for Vertical    # <- ToDo: print title sideways
#   "h" for Horizontal
#   "d" for Deskbar
#
# Default:
# orientation = "h"
orientation = "h"

# Icons of programs have `icon_size`
# Buttons (minimize, maximize, close) have `mini_icon_size`
icon_size = 48
#mini_icon_size = 32
mini_icon_size = 40

#####################################
# Automatic settings block 1 start ->
#
# Width & Height:
if orientation != "v":
    w = 128             # GTK makes it wider if necessary
    h = icon_size
else:
    w = icon_size
    h = 128             # Due to GTK algorithms it moght expand
# <- Automatic settings block 1 end
###################################

# <- Rewrite settings manually if necessary:
# w =
# h =

# Gravity (or rather which side of the screen to attach to)
# and orientation
# Default:
# gravity = "NORTH_EAST"
# orientation == "h"
#
# Setting gravity to *EAST will attach this program to right
# Setting gravity to *SOUTH will attach this program to bottom
#
gravity = "NORTH_EAST"  # top right corner
orientation == "h"      # horizontal

# <- Rewrite settings manually if necessary:

#####################################
# Automatic settings block 2 start ->
#
# xpos              \ position on screen
# ypos              /
# autohide_dir      in which direction to hide it
# Autohide direction can be "U" (up), "D" (down), "R" (right), "L" (left)
# Default:
# autohide_dir = "U"
#
if orientation == "h":
    if gravity.endswith("SOUTH"):
        autohide_dir = "D"
    else:
        autohide_dir = "U"
else:
    if gravity.endswith("EAST"):
        autohide_dir = "R"
    else:
        autohide_dir = "L"
#
if gravity == "NORTH_EAST":
    if orientation == "h":
        autohide_dir == "U"
        xpos = -120 # <- Plus the screen width
        ypos = -1   # <- This to avoid flickering when autohide + hover
    else:
        autohide_dir == "R"
        xpos = -48  # <- Plus the screen width
        ypos = 48
# <- Automatic settings block 2 end
###################################

# Rewrite settings manually if necessary:
# (-1 instead of 0 helps against autohide flickering)

# autohide_dir =
# xpos =
# ypos =

# Autohide
# Default:
# autohide = True
# (`True` and `False` in Python should be exactly capitalized like this)
autohide = True

# Autohide gap (a tiny strip visible at the edge of screen when autohide)
# Default (pixels)
# gap = 3
gap = 3

# Shown on all desktop workplaces?
# (`True` and `False` in Python should be exactly capitalized like this)
sticky = True

# For currently active program, whether to show: icon, title, controls
# (And should it all be aligned start/end (left/right, top/bottom)?
#
# Defaults:
# show_icon = True
# show_title = False
# show_controls = True
# align_end = False

show_icon = True
show_title = False
show_controls = True
align_end = False

#######
# Theme

# If name is None, the current theme set for xfwm4 would be used
# Otherwise try to find and use buttons from xfwm4 theme with `name`
# (case-sensitive, exactly as its folder name). E.g.:
# name = "Agualemon"
#
# Default:
# name = None
name = None

#### ToDo: currently doesn't work:
####
#### Variant can be None or "dark" or whatever there is in the theme
####
#### Note that the background is defined by "dark mode" set for the panels;
#### the theme here is just for buttons' backgrounds.
####
#### variant = "dark"
#### Default:
#### variant = None
#### variant = "dark"
####

dark_mode = False   # False or True. If it's None then
                    # `get_dark_mode` query would be run

###################################################################
# DO NOT CHANGE HERE AND BELOW, UNLESS YOU KNOW WHAT YOU ARE DOING:
#
#       shell command to get the current theme name
#
#       * If `name` is set above and is not None, then this will not run.
#         Otherwise buttons would be searched for as
#         <theme_path>/<name>/xfwm4/<button>-<state>.xpm
#
# Default:
# get_theme_name = "xfconf-query -c xfwm4 -p /general/theme -lv | awk '{print $(NF)}'"

get_theme_name = "xfconf-query -c xfwm4 -p /general/theme -lv | awk '{print $(NF)}'"

#       paths to search for themes ($HOME can be written as "~")
#
# Default:
# theme_paths = [
#    "~/.themes",
#    "/usr/share/themes",
#    "/usr/local/share/themes"
#    ]

theme_paths = [
    "~/.themes",
    "/usr/share/themes",
    "/usr/local/share/themes"
    ]

# Figure out if the panels are in dark mode: run this command, but only
# if (dark_mode == None)
# Default:
# get_dark_mode = "xfconf-query -c xfce4-panel -p /panels/dark-mode -lv | awk '{print $(NF)}'"

get_dark_mode = "xfconf-query -c xfce4-panel -p /panels/dark-mode -lv | awk '{print $(NF)}'"
